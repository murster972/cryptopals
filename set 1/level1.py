#!/usr/bin/env python3
import sys

''' convert hex to base 64 - http://cryptopals.com/sets/1/challenges/1
    :NOTE: can only work with bytes cannot use strings while converting
           only to print '''

def hex_to_bytes(hex_str):
    ''' convert hex str to bytes '''
    hex_chars = "0123456789ABCDEF"

    # check if valid hex
    for char in hex_str:
        if char.upper() not in hex_chars:
            print("INVALID HEX STRING: {}".format(hex_str))
            sys.exit(1)

    #NOTE: will only work - for obivous reasons - if hex chars are in pairs
    return bytes.fromhex(hex_str)

def base64(bytes_):
    # seperate bytes into six-bit blocks - use bitwise operations
    sextets = []

    # bits from previous byte
    prev = 0
    prev_l = 0

    for ind in range(len(bytes_)):
        byte = bytes_[ind]

        # bits left-over from previous byte
        # calc bytes needed from this and get them
        size = 6 - prev_l
        lower_bits = byte >> (8 - size)

        # shift previous bits so that they can be added with current to form
        # six bit block
        upper_bits = prev << size

        block = lower_bits + upper_bits
        sextets.append(block)


        # if six-bits left over add the to sextets else add to prev
        if size == 2:
            lower_six = byte & 63
            sextets.append(lower_six)
            prev_l = 0
            prev = 0

        else:
            leftover_size = 8 - size
            and_val_bin = ("0" * (8 - leftover_size)) + ("1" * leftover_size)
            leftover_bits = byte & int(and_val_bin, 2)
            prev = leftover_bits
            prev_l = leftover_size


    # left-over bits
    if prev_l != 0:
        padd = 6 - prev_l
        bits = prev << padd
        sextets.append(bits)

    base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    # sextets to base64 chars
    b64 = "".join([base64_chars[i] for i in sextets])

    # padds base64 chars with "=" so in blocks of 4 chars
    if len(b64) % 4 != 0:
        padding = "=" * (4 - (len(b64) % 4))
        b64 += padding

    return b64

def main():
    hex_str = sys.argv[1]
    bytes_ = hex_to_bytes(hex_str)

    b = base64(bytes_)

    print(b == "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t")

if __name__ == '__main__':
    main()
